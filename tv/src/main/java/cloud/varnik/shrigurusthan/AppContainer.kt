package cloud.varnik.shrigurusthan

import android.content.Context
import com.google.android.exoplayer2.database.DatabaseProvider
import com.google.android.exoplayer2.database.StandaloneDatabaseProvider
import com.google.android.exoplayer2.offline.DownloadManager
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import com.google.android.exoplayer2.upstream.cache.Cache
import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor
import com.google.android.exoplayer2.upstream.cache.SimpleCache
import java.io.File
import java.util.concurrent.Executor


class AppContainer(context: Context) {
    var dataSourceFactory = DefaultHttpDataSource.Factory()
    private  var databaseProvider: DatabaseProvider = StandaloneDatabaseProvider(context)
    private var downloadDirectory: File = File(context.getExternalFilesDir(null), "my app")
    var downloadCache: Cache = SimpleCache(
        downloadDirectory,
        NoOpCacheEvictor(),
        databaseProvider
    )
    var downloadManager: DownloadManager

    init {
        val downloadExecutor = Executor { obj: Runnable -> obj.run() }
        downloadManager = DownloadManager(
            context,
            databaseProvider,
            downloadCache,
            dataSourceFactory,
            downloadExecutor
        )

    }

}