package cloud.varnik.shrigurusthan

import android.app.AlertDialog
import android.content.DialogInterface
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.PlaybackException
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.offline.Download
import com.google.android.exoplayer2.offline.DownloadCursor
import com.google.android.exoplayer2.offline.DownloadHelper
import com.google.android.exoplayer2.offline.DownloadService
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.StyledPlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.cache.CacheDataSource
import com.google.android.exoplayer2.util.Util
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Streaming
import retrofit2.http.Url
import java.io.IOException


class VideoPlayerFragment : Fragment() {
    private var player: ExoPlayer? = null
    var playerView : StyledPlayerView? = null
    var textView : TextView? = null
    var gDriveFiles = DriveFiles()
    companion object {
        val TAG = "VideoPlayerFragment"
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.video_player_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val trackSelector = DefaultTrackSelector(requireContext())
        trackSelector.setParameters(
            trackSelector.buildUponParameters().setMaxVideoSizeSd())

        playerView = view.findViewById(R.id.playerView)
        textView = view.findViewById(R.id.textView)

        val apiClient = APIClient.getInstance()
        apiClient.getDriveFiles().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<DriveFiles> {
                override fun onNext(driveFiles: DriveFiles) {
                    Log.d("API","onNext")
                    gDriveFiles = driveFiles

                    for(data in driveFiles.files!!){
                        Log.d("API",data!!.id!!)
                        downloadMedia(data)
                    }

                }
                override fun onError(e: Throwable) {
                    Log.d("API","onError")
                    updateStub("Error Retriving Data from Google Drive")
                }
                override fun onComplete() {
                    Log.d("API","onComplete")
                }
                override fun onSubscribe(d: Disposable) {
                    Log.d("API","onSubscribe")
                }
            })

        (requireActivity().application as MyApp).appContainer.downloadManager.addListener(object : com.google.android.exoplayer2.offline.DownloadManager.Listener {
            override fun onDownloadRemoved(downloadManager: com.google.android.exoplayer2.offline.DownloadManager, download: Download) {
                // Toast.makeText(context, "Deleted", Toast.LENGTH_SHORT).show()
                Log.d(TAG,"onDownloadRemoved")
            }

            override fun onDownloadsPausedChanged(downloadManager: com.google.android.exoplayer2.offline.DownloadManager, downloadsPaused: Boolean) {
                Log.d(TAG,"onDownloadsPausedChanged")
            }

            override fun onDownloadChanged(
                downloadManager: com.google.android.exoplayer2.offline.DownloadManager,
                download: Download,
                finalException: Exception?
            ) {
                updateStub("Inprogress : ${downloadManager.currentDownloads.size} Progress : ${download.percentDownloaded}")
                if((downloadManager.currentDownloads.size == 0)){
                    playOfflineVideos()
                }
                Log.d(TAG,"onDownloadChanged::${downloadManager.currentDownloads.size}"+download.percentDownloaded)
            }
        })

    }

    fun playOfflineVideos(){
        playerView!!.visibility = View.VISIBLE
        textView!!.visibility = View.GONE
        Log.d(TAG,"playOfflineVideos")
        val cacheDataSourceFactory: DataSource.Factory = CacheDataSource.Factory()
            .setCache((requireActivity().application as MyApp).appContainer.downloadCache)
            .setUpstreamDataSourceFactory((requireActivity().application as MyApp).appContainer.dataSourceFactory)
            .setCacheWriteDataSinkFactory(null) // Disable writing.

        player = ExoPlayer.Builder(requireContext())
            .setMediaSourceFactory(
                DefaultMediaSourceFactory(cacheDataSourceFactory)
            )
            .build()

        player!!.addListener(object : Player.Listener {
            override fun onPlayerError(error: PlaybackException) {
                super.onPlayerError(error)
                showDialog(error!!.localizedMessage!!)
            }
        })

        for(media in getDownloadedItems()){
            for(driveFiles in gDriveFiles.files!!){
                if(media.url!!.contains(driveFiles!!.id!!)){
                    val mediaSources = ProgressiveMediaSource.Factory(cacheDataSourceFactory).createMediaSource(
                        MediaItem.fromUri(Uri.parse(media.url!!)))
                    player!!.addMediaSource(mediaSources)
                }
            }
        }

        playerView!!.player = player
        player!!.repeatMode = Player.REPEAT_MODE_ALL
        player!!.playWhenReady = true
        player!!.play()

    }

    private fun updateStub(data : String){
        textView!!.text = data
    }

    private fun getDownloadedItems(): MutableList<Media> {
        val downloadedTracks= ArrayList<Media>()
        val downloadCursor: DownloadCursor =(context?.applicationContext as MyApp).appContainer.downloadManager.downloadIndex.getDownloads()
        if (downloadCursor.moveToFirst()) {
            do{
                val jsonString = Util.fromUtf8Bytes(downloadCursor.download.request.data)
                val jsonObject = JSONObject(jsonString)
                val uri = downloadCursor.download.request.uri

                downloadedTracks.add(
                    Media(
                        url = uri.toString(),
                        name = jsonObject.getString("name")
                        ))
            }while (downloadCursor.moveToNext())
        }
        return downloadedTracks
    }
    private fun downloadMedia( data : FilesItem){

        //the download request
        val helper = context?.let { DownloadHelper.forProgressive(it, Uri.parse("https://drive.google.com/uc?export=download&id=${data!!.id}")) }
        helper?.prepare(object : DownloadHelper.Callback {
            override fun onPrepared(helper: DownloadHelper) {
                val json = JSONObject()
                //extra data about the download like title, artist e.tc below is an example
                json.put("name" ,data.name)
                val download =
                    helper.getDownloadRequest(data.id!!, Util.getUtf8Bytes(json.toString()))
                //sending the request to the download service
                context?.let {
                    DownloadService.sendAddDownload(
                        it,
                        MyDownload::class.java,
                        download,
                        true
                    )
                }
            }

            override fun onPrepareError(helper: DownloadHelper, e: IOException) {
                e.printStackTrace()
                Toast.makeText(context, e.localizedMessage, Toast.LENGTH_SHORT).show()
            }
        })

    }

    fun showDialog( text : String){
        val builder1: AlertDialog.Builder = AlertDialog.Builder(context)
        builder1.setMessage(text)
        builder1.setCancelable(true)

        builder1.setPositiveButton(
            "Yes",
            DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })

        builder1.setNegativeButton(
            "No",
            DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })

        val alert11: AlertDialog = builder1.create()
        alert11.show()
    }
}

interface APIService {
    @GET
    fun getDriveFiles(@Url uri: Uri) : Observable<DriveFiles>


    @Streaming
    @GET
    fun downloadFileByUrlRx(@Url fileUrl: String): Observable<Response<ResponseBody>>

}
