package cloud.varnik.shrigurusthan

data class DriveFiles(
	val kind: String? = null,
	val files: List<FilesItem?>? = null,
	val incompleteSearch: Boolean? = null
)

data class FilesItem(
	val kind: String? = null,
	val name: String? = null,
	val id: String? = null,
	val mimeType: String? = null
)


data class Media(
	val url: String? = null,
	val name: String? = null
)
