package cloud.varnik.shrigurusthan

import android.app.Notification
import android.content.Context
import com.google.android.exoplayer2.offline.Download
import com.google.android.exoplayer2.offline.DownloadManager
import com.google.android.exoplayer2.offline.DownloadService
import com.google.android.exoplayer2.scheduler.Scheduler
import com.google.android.exoplayer2.ui.DownloadNotificationHelper


class MyDownload: DownloadService(1, DEFAULT_FOREGROUND_NOTIFICATION_UPDATE_INTERVAL, "my app", R.string.app_name, R.string.app_name){

    private lateinit var notificationHelper: DownloadNotificationHelper
    private lateinit var context: Context

    override fun onCreate() {
        super.onCreate()
        context = this
        notificationHelper= DownloadNotificationHelper(this, "my app")

    }
    override fun getDownloadManager(): DownloadManager {
        val manager = (application as MyApp).appContainer.downloadManager
        //Set the maximum number of parallel downloads
        manager.maxParallelDownloads = 5
       return manager
    }


    override fun getScheduler(): Scheduler? {
        return null
    }

    override fun getForegroundNotification(
        downloads: MutableList<Download>,
        notMetRequirements: Int
    ): Notification {
        return notificationHelper.buildProgressNotification(context,R.drawable.app_logo, null, getString(R.string.app_name), downloads)
    }
}