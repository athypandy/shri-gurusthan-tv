package cloud.varnik.shrigurusthan

import android.net.Uri
import io.reactivex.Observable
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class APIClient {

    private val apiService   =  Retrofit.Builder()
        .baseUrl("https://googledrive.com")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(getHttpClient())
        .build().create(APIService::class.java)

    companion object{
        lateinit var INSTANCE: APIClient
        fun getInstance(): APIClient {
            INSTANCE = APIClient()
            return INSTANCE
        }
    }


    fun getDriveFiles(): Observable<DriveFiles> {
        return apiService.getDriveFiles(Uri.parse("https://www.googleapis.com/drive/v3/files?q=%221-n7PXWuTn5vjF6EfhFKxWXaO4oqwJkv8%22%20in%20parents&key=AIzaSyDLriQaFlcrEQVCdc5M_tlg8EgV4mgBxGI"))
    }
    private fun getHttpClient(): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        return OkHttpClient()
            .newBuilder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(Interceptor { chain ->
                val request: Request = chain.request()
                val newRequest: Request = request.newBuilder()
                    .build()
                chain.proceed(newRequest)
            })
            .build()
    }
}