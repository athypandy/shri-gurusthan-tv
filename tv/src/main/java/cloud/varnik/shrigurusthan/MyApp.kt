package cloud.varnik.shrigurusthan

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build

class MyApp : Application() {
    lateinit var appContainer: AppContainer
    var ID = "My app"

    override fun onCreate() {
        super.onCreate()
        appContainer = AppContainer(this)
        createNotificationChannels()
    }

    private fun createNotificationChannels() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(ID, getString(R.string.app_name), NotificationManager.IMPORTANCE_HIGH )
            channel.description = getString(R.string.app_name)
            val manager = getSystemService(NotificationManager::class.java)
            manager.createNotificationChannel(channel)
        }
    }
}